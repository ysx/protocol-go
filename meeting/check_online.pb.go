// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.23.0
// 	protoc        v3.14.0
// source: meeting/check_online.proto

package meeting

import (
	proto "github.com/golang/protobuf/proto"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

type CheckOnlineReq struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 编号
	// @inject_tag: validate:"required"
	Id int64 `protobuf:"varint,10,opt,name=id,proto3" json:"id,omitempty" validate:"required"`
	// 手机号
	// @inject_tag: validate:"required"
	Mobile string `protobuf:"bytes,20,opt,name=mobile,proto3" json:"mobile,omitempty" validate:"required"`
}

func (x *CheckOnlineReq) Reset() {
	*x = CheckOnlineReq{}
	if protoimpl.UnsafeEnabled {
		mi := &file_meeting_check_online_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CheckOnlineReq) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CheckOnlineReq) ProtoMessage() {}

func (x *CheckOnlineReq) ProtoReflect() protoreflect.Message {
	mi := &file_meeting_check_online_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CheckOnlineReq.ProtoReflect.Descriptor instead.
func (*CheckOnlineReq) Descriptor() ([]byte, []int) {
	return file_meeting_check_online_proto_rawDescGZIP(), []int{0}
}

func (x *CheckOnlineReq) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *CheckOnlineReq) GetMobile() string {
	if x != nil {
		return x.Mobile
	}
	return ""
}

type CheckOnlineRsp struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 是否在线
	IsOnline bool `protobuf:"varint,10,opt,name=isOnline,proto3" json:"isOnline,omitempty"`
}

func (x *CheckOnlineRsp) Reset() {
	*x = CheckOnlineRsp{}
	if protoimpl.UnsafeEnabled {
		mi := &file_meeting_check_online_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CheckOnlineRsp) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CheckOnlineRsp) ProtoMessage() {}

func (x *CheckOnlineRsp) ProtoReflect() protoreflect.Message {
	mi := &file_meeting_check_online_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CheckOnlineRsp.ProtoReflect.Descriptor instead.
func (*CheckOnlineRsp) Descriptor() ([]byte, []int) {
	return file_meeting_check_online_proto_rawDescGZIP(), []int{1}
}

func (x *CheckOnlineRsp) GetIsOnline() bool {
	if x != nil {
		return x.IsOnline
	}
	return false
}

var File_meeting_check_online_proto protoreflect.FileDescriptor

var file_meeting_check_online_proto_rawDesc = []byte{
	0x0a, 0x1a, 0x6d, 0x65, 0x65, 0x74, 0x69, 0x6e, 0x67, 0x2f, 0x63, 0x68, 0x65, 0x63, 0x6b, 0x5f,
	0x6f, 0x6e, 0x6c, 0x69, 0x6e, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x07, 0x6d, 0x65,
	0x65, 0x74, 0x69, 0x6e, 0x67, 0x22, 0x38, 0x0a, 0x0e, 0x43, 0x68, 0x65, 0x63, 0x6b, 0x4f, 0x6e,
	0x6c, 0x69, 0x6e, 0x65, 0x52, 0x65, 0x71, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x0a, 0x20,
	0x01, 0x28, 0x03, 0x52, 0x02, 0x69, 0x64, 0x12, 0x16, 0x0a, 0x06, 0x6d, 0x6f, 0x62, 0x69, 0x6c,
	0x65, 0x18, 0x14, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x6d, 0x6f, 0x62, 0x69, 0x6c, 0x65, 0x22,
	0x2c, 0x0a, 0x0e, 0x43, 0x68, 0x65, 0x63, 0x6b, 0x4f, 0x6e, 0x6c, 0x69, 0x6e, 0x65, 0x52, 0x73,
	0x70, 0x12, 0x1a, 0x0a, 0x08, 0x69, 0x73, 0x4f, 0x6e, 0x6c, 0x69, 0x6e, 0x65, 0x18, 0x0a, 0x20,
	0x01, 0x28, 0x08, 0x52, 0x08, 0x69, 0x73, 0x4f, 0x6e, 0x6c, 0x69, 0x6e, 0x65, 0x42, 0x11, 0x5a,
	0x0f, 0x6d, 0x65, 0x65, 0x74, 0x69, 0x6e, 0x67, 0x3b, 0x6d, 0x65, 0x65, 0x74, 0x69, 0x6e, 0x67,
	0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_meeting_check_online_proto_rawDescOnce sync.Once
	file_meeting_check_online_proto_rawDescData = file_meeting_check_online_proto_rawDesc
)

func file_meeting_check_online_proto_rawDescGZIP() []byte {
	file_meeting_check_online_proto_rawDescOnce.Do(func() {
		file_meeting_check_online_proto_rawDescData = protoimpl.X.CompressGZIP(file_meeting_check_online_proto_rawDescData)
	})
	return file_meeting_check_online_proto_rawDescData
}

var file_meeting_check_online_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_meeting_check_online_proto_goTypes = []interface{}{
	(*CheckOnlineReq)(nil), // 0: meeting.CheckOnlineReq
	(*CheckOnlineRsp)(nil), // 1: meeting.CheckOnlineRsp
}
var file_meeting_check_online_proto_depIdxs = []int32{
	0, // [0:0] is the sub-list for method output_type
	0, // [0:0] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_meeting_check_online_proto_init() }
func file_meeting_check_online_proto_init() {
	if File_meeting_check_online_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_meeting_check_online_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CheckOnlineReq); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_meeting_check_online_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CheckOnlineRsp); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_meeting_check_online_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   2,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_meeting_check_online_proto_goTypes,
		DependencyIndexes: file_meeting_check_online_proto_depIdxs,
		MessageInfos:      file_meeting_check_online_proto_msgTypes,
	}.Build()
	File_meeting_check_online_proto = out.File
	file_meeting_check_online_proto_rawDesc = nil
	file_meeting_check_online_proto_goTypes = nil
	file_meeting_check_online_proto_depIdxs = nil
}
